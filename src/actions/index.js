import Alt from '../alt';
import Firebase from 'firebase';
import _ from 'lodash';

class Actions {
  
  initSession() {
    return (dispatch) => {
      let firebaseRef = new Firebase('https://wally-hunt.firebaseio.com');
      let authData = firebaseRef.getAuth();
      let user;
      if (authData) 
        user = {
              id: authData.facebook.id,
              name: authData.facebook.displayName,
              avatar: authData.facebook.profileImageURL
        };
      else
        user = null;
      console.log(user);
      dispatch(user);
    }
  }
  
  login() {
    return (dispatch) => {
      let firebaseRef = new Firebase('https://wally-hunt.firebaseio.com');
      firebaseRef.authWithOAuthPopup('facebook', (error, fbProfile) => {
        if (error) {
          console.log('Login failed', error);
          return;
        }    
        console.log('Login successful', fbProfile);
        let user = {
          id: fbProfile.facebook.id,
          name: fbProfile.facebook.displayName,
          avatar: fbProfile.facebook.profileImageURL
        };
        firebaseRef.child('users').child(fbProfile.facebook.id).set(user);
        setTimeout(() => dispatch(user));
      });
    }
  }
  
  logout() {
    return (dispatch) => {
      let firebaseRef = new Firebase('https://wally-hunt.firebaseio.com');
      firebaseRef.unauth();
      setTimeout(() => dispatch(null));
    }
  }
  
  getProducts() {
    return (dispatch) => {
      let productRef = new Firebase('https://wally-hunt.firebaseio.com/products');
      productRef.on('value',  snapshot => {
        let productsVal = snapshot.val();
        let products = _(productsVal).keys().map( productKey => {
          let product = _.clone(productsVal[productKey]);
          product.key = productKey;
          return product;
        })
        .value();
        console.log(products);
        dispatch(products);
      });
    }
  }
  
  addProduct(product) {
    return (dispatch) => {
      let productRef = new Firebase('https://wally-hunt.firebaseio.com/products');
      productRef.push(product);
    }
  }
  
  addVote(productId, userId) {
    return (dispatch) => {
      let firebaseRef = new Firebase('https://wally-hunt.firebaseio.com');
      let voteRef = firebaseRef.child('votes').child('productId').child(userId);
      voteRef.on('value', snapshot => {
        if (snapshot.val() == null) {
          voteRef.set(true);
          firebaseRef = firebaseRef.child('products').child(productId).child('upvote');
          let vote = 0;
          firebaseRef.on('value', snapshot => {
            vote = snapshot.val();
          });
          firebaseRef.set(vote+1);
        }
      });
    }
  }
  
  addComment(productId, comment) {
    return (dispatch) => {
      let commentRef = new Firebase('https://wally-hunt.firebaseio.com/comments');
      commentRef.child(productId).push(comment);
    }
  }
  
  getComments(productId) {
    return (dispatch) => {
      let commentRef = new Firebase('https://wally-hunt.firebaseio.com/comments');
      commentRef.child(productId).on('value', snapshot => {
        let commentsVal = snapshot.val();
        let comments = _(commentsVal).keys().map( commentKey => {
          var comment = _.clone(commentsVal[commentKey]);
          comment.key = commentKey;
          return comment;
        })
        .value();
        dispatch(comments);
      });
    }
  }
}

export default Alt.createActions(Actions);