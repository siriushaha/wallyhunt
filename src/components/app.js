import React, { Component } from 'react';

import MainPage from './home/main_page';
import MainBar from './home/main_bar';

import connectToStores from 'alt-utils/lib/connectToStores';
import ProductStore from '../stores/ProductStore';
import Actions from '../actions';

@connectToStores
export default class App extends Component {
  constructor() {
    super();
    Actions.initSession();
  }
  
  static getStores() {
    return [ProductStore];
  }
  
  static getPropsFromStores() {
    return ProductStore.getState();
  }
  
  render() {
    return (
      <div>
        <MainBar user={this.props.user}/>
        <MainPage />
      </div>
    );
  }
}
