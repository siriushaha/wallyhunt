import React, { Component } from 'react';
import Actions from '../../actions';
import connectToStores from 'alt-utils/lib/connectToStores';
import ProductStore from '../../stores/ProductStore';

@connectToStores
class UpVote extends Component {
  
    static getStores() {
      return [ProductStore];
    }
    
    static getPropsFromStores() {
      return ProductStore.getState();    
    }

    upVote() {
      Actions.addVote(this.props.pid, this.props.user.id);
    }

    render() {
      return (
        <a className="upvote-button" href="#" onClick={this.upVote.bind(this)}>
          <span>
            <i className="fa fa-sort-asc" />
          </span>
          <br/>
          {this.props.product.upvote}
        </a>            
      )
    }  
  
}

export default UpVote;