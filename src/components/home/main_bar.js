import React, { Component } from 'react';
import LoginPopup from '../popup/login';
import ProductPopup from '../popup/product';
import ProfileMenu from './profile_menu';

class MainBar extends Component {
    constructor(){
        super();
        this.state = {
            popupStatus: false
        }
    }
    
    showPopup()  {
        this.setState({ popupStatus: true });
    }
    
    hidePopup() {
        this.setState({ popupStatus: false });
    }
    
    renderProductSearch() {
        return (
            <section className="left-side">
                <input type="text" className="product-search" placeholder="Enter search product" />
            </section>
        )
    }
    
    renderLogo() {
        return (
            <a href="#"><img src="/img/favicon.ico" /></a>
        )
    }
    
    renderProductPopup() {
        return (
            <section>
                <span>
                    <a href="#" onClick={this.showPopup.bind(this)} className="post-btn">Post</a>
                    <ProfileMenu user={this.props.user}/>
                </span>
                <ProductPopup user={this.props.user} status={this.state.popupStatus} hidePopup={this.hidePopup.bind(this)} />
            </section>
        )
    }
    
    renderLoginPopup() {
        return (
            <section>
                <a href="#" onClick={this.showPopup.bind(this)} className="login-btn">Login</a>
                <LoginPopup status={this.state.popupStatus} hidePopup={this.hidePopup.bind(this)} />
            </section>
        )        
    }
    
    renderLoginOrProduct() {
        const isLoggedIn = this.props.user;
        return (
            <section className="right-side">
                {isLoggedIn ? this.renderProductPopup() : this.renderLoginPopup()}
            </section>
        )    
    }
    
    render() {
        return (
            <section className="navbar">
                {this.renderProductSearch()}
                {this.renderLogo()}
                {this.renderLoginOrProduct()}
            </section>
        )
    }
}

export default MainBar;
