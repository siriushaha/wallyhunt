import React, { Component } from 'react';
//import Firebase from 'firebase';
import connectToStores from 'alt-utils/lib/connectToStores';
import ProductStore from '../../stores/ProductStore';
import Actions from '../../actions';
import ProductList from '../products/product_list';

@connectToStores
class MainPage extends Component {
    
    static getStores() {
        return [ProductStore];
    }
    
    static getPropsFromStores() {
        return ProductStore.getState();
    }
    
    constructor() {
        super();
        this.state = {
            products: []
        };
        Actions.getProducts();
    }
    
    componentWillMount() {
        //Actions.getProducts();
    }
    
    render() {
        return (
            <section>
                <header>
                    <img src="/img/banner.jpeg" width="100%" />
                </header>
                <ProductList products={this.props.products} />
            </section>
        )
    }
}

export default MainPage;