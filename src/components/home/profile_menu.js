import React, { Component } from 'react';
import Actions from '../../actions';

class ProfileMenu extends Component {
  
  constructor() {
    super();
    this.state = {
      showProfileMenu: false
    }
  }
  
  clickOnAvatar(event) {
      this.setState({showProfileMenu: !this.state.showProfileMenu});
  }
  
  clickOutsideAvatar(event) {
    if (event.target != this.refs.profileAvatar)
      this.setState( {showProfileMenu: false} );
  }
  
  clickToLogout(event) {
    event.preventDefault();
    Actions.logout();
  }
  
  componentWillMount() {
    window.addEventListener('click', this.clickOutsideAvatar.bind(this), false);
  }
  componentWillUnMount() {
    window.removeEventListener('click', this.clickOutsideAvatar.bind(this), false);
  }
  
  renderProfileNav() {
    return (
      <nav className="profile-nav" ref="profileNav">
        <a href="#">My Profile</a>
        <a href="#" onClick={this.clickToLogout.bind(this)}>Logout</a>
      </nav>
    )
  }
  
  render() {
    const onProfileMenu = this.state.showProfileMenu;
    return (
      <section className="profile-menu">
        <img src={this.props.user.avatar} onClick={this.clickOnAvatar.bind(this)} className="profile-btn medium-avatar" ref="profileAvatar" />
        {onProfileMenu ? this.renderProfileNav() : null}
      </section>
    )
  }
  
}

export default ProfileMenu;
