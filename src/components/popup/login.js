import React, { Component } from 'react';
import Popup from '../common/popup';

import Actions from '../../actions';

class LoginPopup extends Component {
    login2Facebook() {
        Actions.login();
        this.props.hidePopup();
    }
    
    render() {
        return (
            <Popup {...this.props} style="login-popup">
                <img src="/img/kitty.png" />
                <h1>Login to join the Community</h1>
                <p>CodeHunt is the Community to share and geek out about the latest codes, news and postcasts Join us.</p>
                <button className="facebook-btn" onClick={this.login2Facebook.bind(this)} >Login with Facebook</button>
                <p>We will never post to Facebook without your permission</p>
            </Popup>
        )
    }
}

export default LoginPopup;