import React, { Component } from 'react';
import Popup from '../common/popup';
//import connectToStores from 'alt-utils/lib/connectToStores';
//import ProductStore from '../../stores/ProductStore';
import Actions from '../../actions';

class ProductPopup extends Component {
  
  addProduct() {
    //event.preventDefault();
    let product = {
      name: this.refs.name.value,
      description: this.refs.description.value,
      link: this.refs.link.value,
      media: this.refs.media.value,
      upvote: 0,
      maker: {
        name: this.props.user.name,
        avatar: this.props.user.avatar
      }
    }
    Actions.addProduct(product);
  }
  
  render() {
    return (
      <Popup {...this.props} style="post-popup">
        <header className="post-header">Post a new product</header>
        <section>
          <table>
            <tbody>
              <tr>
                <td>Name</td>
                <td><input placeholder="Enter product name" ref="name" /></td>
              </tr>
              <tr>
                <td>Description</td>
                <td><input placeholder="Enter product description" ref="description" /></td>
              </tr>
              <tr>
                <td>Link</td>
                <td><input placeholder="http://www..." ref="link" /></td>
              </tr>
              <tr>
                <td>Media</td>
                <td><input placeholder="Paste a link to an product image" ref="media"/></td>
              </tr>
            </tbody>
          </table>
        </section>
        <footer className="post-footer">
          <button className="post-btn" onClick={this.addProduct.bind(this)}>Add Product</button>
        </footer>
      </Popup>
    )
  }
}

export default ProductPopup;