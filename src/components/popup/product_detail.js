import React, { Component } from 'react';
import Popup from '../common/popup';
import UpVote from '../common/up_vote';

import Actions from '../../actions';
import connectToStores from 'alt-utils/lib/connectToStores';
import ProductStore from '../../stores/ProductStore';

@connectToStores
class ProductDetailPopup extends Component {

  static getStores() {
    return [ProductStore];
  }
    
  static getPropsFromStores() {
    return ProductStore.getState();    
  }

  constructor() {
    super();
  }
  
  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.status && this.props.status != nextProps.status) {
      Actions.getComments(this.props.pid);
    }
    return true;
  }
  
  saveComment(e) {
    if (e.keyCode === 13 && e.target.value.length > 0) {
      let comment = { 
        content: e.target.value,
        name: this.props.user.name,
        avatar: this.props.user.avatar
      };
      Actions.addComment(this.props.pid, comment);
      e.target.value = null;
    }
  }

  renderHeader(product) {
    return (
      <header style={{backgroundImage: `url(${product.media})`}}>
        <section className="header-shadow">
          <h1>{product.name}</h1>
          <p>{product.description}</p>
          <section>
            <UpVote product={product} />
            <a className="getit-btn" href={product.link} target="_blank">Get It</a>
          </section>
        </section>
      </header>
    )
  }
  
  renderBodyDiscussion(comments) {
    return (
      <section className="discussion">
        <h2>Discussion</h2>
        {
          this.props.user
          ?
          <section className="post-comment">
            <img className="medium-avatar" src={this.props.user.avatar} />
            <input placeholder="What do you think of this" onKeyUp={this.saveComment.bind(this)}/>
          </section>
          :
          null
        }
        {this.renderComments(comments)}
      </section>
    )
  }

  renderBody(comments) {
    return (
      <section className="product-popup-body">
        <main>>
          {this.renderBodyDiscussion(comments)}
        </main>
      </section>
    )
  }
  
  renderComments(comments) {
    return (
      <ul className="comment-list">
        {
          comments.map((comment,idx) => {
            return (
              <li key={idx}>
                <img className="medium-avatar" src={comment.avatar} />
                <section>
                  <strong>{comment.name}</strong>
                  <p>{comment.content}</p>
                </section>
              </li>
            )
          })
        }
      </ul>
    )
  }
  
  render() {
    //const { comments }  = this.state;
    const { product, comments } = this.props;
    return (
      <Popup {...this.props} style="product-popup">
        {this.renderHeader(product)}
        {this.renderBody(comments)}
      </Popup>
    )
  }
}

export default ProductDetailPopup;