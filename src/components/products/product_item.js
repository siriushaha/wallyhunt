import React, { Component } from 'react';
import ProductDetailPopup from '../popup/product_detail';
import UpVote from '../common/up_vote';

class ProductItem extends Component {
    constructor() {
        super();
        this.state = {
            popupStatus: false
        }
    }
    
    showProductDetail() {
        this.setState( { popupStatus: true });
    }
    
    hideProductDetail() {
        this.setState( { popupStatus: false });
    }
    
    render() {
        const { product } = this.props;
        return (
            <li className="product-item" >
                <UpVote product={product}/>
                {this.renderMedia(product.media)}
                {this.renderInfo(product.name, product.description, product.maker.avatar)}
                {this.renderLink(product.link)}
                <ProductDetailPopup {...this.props} status={this.state.popupStatus} hidePopup={this.hideProductDetail.bind(this)} />
            </li>
        );
    }

    renderMedia(media) {
        return (
            <img className="product-item-media" src={media} />        
        )    
    }

    renderInfo(name, description, avatar) {
        return (
            <section className="product-item-info">
                <a href="#" onClick={this.showProductDetail.bind(this)}>
                    <h2>{name}</h2>
                </a>
                <p>{description}</p>
                <a href="#">
                    <img className="small-avatar" src={avatar} />
                </a>
            </section>
        )
    }

    renderLink(link) {
        return (
            <a className="product-item-link" href={link}>
                <span>
                    <i className="fa fa-external-link" />
                </span>
            </a>
        )        
    }

};

export default ProductItem;