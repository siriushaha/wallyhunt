import React from 'react';

import ProductItem from './product_item';

const ProductList = ( {products} ) => {
    if (!products) {
        return (
            <section>
                <p>No products</p>
            </section>
        )   
    }

    const productList = products.map( (product, idx)  => {
            return (
                <ProductItem key={idx} pid={product.key} product={product} />    
            )
        });

    return (
        <section className="container">
            <ul className="product-list">
                {productList}
            </ul>
        </section>
    )
}

export default ProductList;