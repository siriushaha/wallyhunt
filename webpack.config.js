module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    path: __dirname,
    publicPath: '/',
    filename: 'wallyhunt.js',
    sourceMapFilename: './wallyhunt.map'
  },
  devtool: '#source-map',
  module: {
    loaders: [{
      exclude: /node_modules/,
      loader: 'babel?plugins[]=babel-plugin-transform-decorators-legacy'
    }]
  },
  resolve: {
    extensions: ['', '.js', '.jsx']
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './'
  }
};
